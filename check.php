<?php
	include 'functions.php';
	
	$path = $_GET["source"];
	
	if(!isset($_COOKIE["testCookie"]))
	{
		$dest = buildNewDestUrl($_SERVER, "error.php");
		header("Location: " . $dest);
	}
	else 
	{
		$dest = buildNewDestUrl($_SERVER, $path);
		header("Location: " . $dest);
	}
?>