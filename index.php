<?php
include 'functions.php';
if (! isset ( $_COOKIE ['testCookie'] )) {
	setcookie ( 'testCookie', 'enabled' );
	$dest = buildNewDestUrlSource ( $_SERVER, "check.php" );
	header ( "Location: " . $dest );
}
if(isset($_COOKIE["firstName"]))	//not first access to this page
{
	if (! checkSession ()) {
		$dest = buildNewDestUrl ( $_SERVER, "index.php" );
		header ( "Location: " . $dest );
	}
}
// if(isset($_COOKIE["source"]))
// {
// 	if(!strcmp($_COOKIE["source"],"statistics.php"))
// 	{
// 		clearCookies();
// 		session_start();
// 		session_unset();
// 		session_destroy();
// 		$dest = buildNewDestUrl( $_SERVER, "index.php" );
// 		header ( "Location: " . $dest );
// 	}
// }
setcookie('source','index.php');
if(isset($_COOKIE["https"]) && $_COOKIE["https"]=="false")
{
	setcookie("https","false",time()-60*60);
	toHttp();	
}
$timeout = false;
if(isset($_COOKIE["timeout"]))
{
	$timeout = true;
	setcookie("timeout","",time() -60*60);
}
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title>Sport Survey</title>
<script type="text/javascript" src="MyScripts.js"></script>
<link href="MyStyle.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="navbar">
		<ul class="navbar">
			<li><a id="navlink" href="index.php">Personal Information</a></li>
			<li><a id="navlink" href="sports.php">Sports</a></li>
			<li><a id="navlink" href="email.php">Email and reward</a></li>			
		</ul>
	</div>
	<div class="header">
		<div id="title">
			<h1>Sport Survey Site</h1>
		</div>
		<div id="page">
			<h2>Personal Information</h2>
		</div>
	</div>
	<noscript id="alert">Sorry, your browser does not support or has disabled
		Javascript! Please consider changing browser or turning it back on.</noscript>
	<div class="content">
	<?php
		if($timeout){
	 		echo('
			<p id="alert">Timeout expired! Please restart completing the survey</p>'			
			); 
		}
	?>
	<form action="validate.php" method="get" class="login" name="personalInfo" onsubmit="return checkPersonalInfo('personalInfo')">
				<h3>Personal Information</h3>
				<label>
					<span>First Name: </span>
					<input type="text" 
						   id="firstName" 
						   name="firstName" 
						   placeholder="Insert your first name" 
						   title="Insert your first name" 
						   value=<?php if(isset($_COOKIE["firstName"]))
						   					echo('"'.$_COOKIE["firstName"].'"');
						   				else 
						   					echo('');
						   		 ?>>
				</label>				
				<label>
					<span>Last Name: </span>
					<input type="text" 
					id="lastName" 
					name="lastName" 
					placeholder="Insert your last name" 
					title="Insert your last name"
					value=<?php if(isset($_COOKIE["lastName"]))
						   					echo('"'.$_COOKIE["lastName"].'"');
						   				else 
						   					echo('');
						   		 ?>>
				</label>
				<label>
					<span>Age: </span>
					<select name="age"	id="age" title="Insert your age">
						<?php 
							$i = 1;
							for($i=1;$i<120;$i++)
							{
								echo('<option ');
								if(isset($_COOKIE["age"]) && $_COOKIE["age"]==$i)
									echo('selected = "true" ');
								echo('value="'.$i.'">'.$i.'</option>
										');
							}
						?>
						
					</select>
				</label>
				<input type="hidden" name="source" value="index.php">
				<input type="submit" id="confirm" value="Confirm">
			</form>

	</div>
</body>
</html>