<?php
include 'functions.php';
if (! isset ( $_COOKIE ['testCookie'] )) {
	setcookie ( 'testCookie', 'enabled' );
	$dest = buildNewDestUrlSource ( $_SERVER, "check.php" );
	header ( "Location: " . $dest );
}
if (! checkSession ()) {
	$dest = buildNewDestUrl ( $_SERVER, "index.php" );
	header ( "Location: " . $dest );
}
setcookie("source","email.php");

if(!isset($_COOKIE["https"]))
{
	setcookie("https","false");
	toHttps();
}
$alert = false;
if(isset($_COOKIE["repeatedEmail"]))
{
	setcookie("repeatedEmail","",time()-60*60);
	$alert = true;
}
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title>Sport Survey</title>
<script type="text/javascript" src="MyScripts.js"></script>
<link href="MyStyle.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="navbar">
		<ul class="navbar">
			<li><a id="navlink" href="index.php">Personal Information</a></li>
			<li><a id="navlink" href="sports.php">Sports</a></li>
			<li><a id="navlink" href="email.php">Email and reward</a></li>			
		</ul>
	</div>
	<div class="header">
		<div id="title">
			<h1>Sport Survey Site</h1>
		</div>
		<div id="page">
			<h2>Email</h2>
		</div>
	</div>
	<noscript id="alert">Sorry, your browser does not support or has disabled
		Javascript! Please consider changing browser or turning it back on.</noscript>
	<div class="content">
	<?php 
		if($alert)
		{
			echo('<p id="alert">Email already registered in the database! Please choose another one.</p><br/>');
		}
	?>
	<form action="validate.php" method="get" class="login" name="emailForm" onsubmit="return checkEmail('emailForm')">
				<h3>Email</h3>
				<label>
					<span>Email: </span>
					<input type="text" id="email" name="email" placeholder="Insert your email" title="Insert your email"
					value=<?php if(isset($_COOKIE["email"]))
						   					echo('"'.$_COOKIE["email"].'"');
						   				else 
						   					echo('');
						   		 ?>>
				</label>
				<input type="hidden" name="source" value="email.php">
				<input type="submit" name="back" id="back" value="Go Back">
				<input type="submit" name="confirm" id="confirm" value="Confirm">
			</form>

	</div>
</body>
</html>