<?php
include 'functions.php';
if (! isset ( $_COOKIE ['testCookie'] )) {
	setcookie ( 'testCookie', 'enabled' );
	$dest = buildNewDestUrlSource ( $_SERVER, "check.php" );
	header ( "Location: " . $dest );
}
if (! checkSession ()) {
	$dest = buildNewDestUrl( $_SERVER, "index.php" );
	header ( "Location: " . $dest );
	exit();
}
session_unset();
session_destroy();
$firstName = $_COOKIE["firstName"];
$lastName = $_COOKIE["lastName"];
$age = $_COOKIE["age"];
$email = $_COOKIE["email"];
$reward = $_COOKIE["reward"];
if(isset($_COOKIE["firstSport"]))
	$firstSport = $_COOKIE["firstSport"];
if(isset($_COOKIE["secondSport"]))
	$secondSport = $_COOKIE["secondSport"];
if(isset($_COOKIE["thirdSport"]))
	$thirdSport = $_COOKIE["thirdSport"];
clearCookies();
setcookie("source","statistics.php");
if(!isset($_COOKIE["https"]))
{
	setcookie("https","false");
	toHttps();
}
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title>Sport Survey</title>
<script type="text/javascript" src="MyScripts.js"></script>
<link href="MyStyle.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="navbar">
		<ul class="navbar">
			<li><a id="navlink" href="index.php">Personal Information</a></li>
			<!-- <li><a id="navlink" href="sports.php">Sports</a></li>
			<li><a id="navlink" href="email.php">Email and reward</a></li>-->
		</ul>
	</div>
	<div class="header">
		<div id="title">
			<h1>Sport Survey Site</h1>
		</div>
		<div id="page">
			<h2>Summary</h2>
		</div>
	</div>
	<noscript id="alert">Sorry, your browser does not support or has disabled
		Javascript! Please consider changing browser or turning it back on.</noscript>
	<div class="content">
	<h4><?php echo($_COOKIE["firstName"]);?> summary</h4>
	<div id="list_content">
		<ul >
			<li><em>First Name:</em> <?php echo($firstName);?></li>
			<li><em>Last Name:</em> <?php echo($lastName);?></li>
			<li><em>Age:</em> <?php echo($age);?></li>
			<?php 
				if(isset($firstSport))
					echo('<li><em>Sport #1:</em> '.$firstSport.'</li>');
				if(isset($secondSport))
					echo('<li><em>Sport #2:</em> '.$secondSport.'</li>');
				if(isset($thirdSport))
					echo('<li><em>Sport #3:</em> '.$thirdSport.'</li>');
			?>
			<li><em>Email:</em> <?php echo($email);?></li>
			<li><em>Reward:</em> <?php echo($reward . " &euro;");?></li>		
		</ul>
	</div>
	<br/>
	<hr>
	<br/>
	<h4>Summary by age</h4>	
	<table class="table">
		<tr id="header">			
			<th>Age</th>
			<th>Number Of Completed Surveys</th>			
		</tr>		
				<?php
					$query = "SELECT COUNT(*) FROM users WHERE users.age >= 0 AND users.age <= 17";
					$res = getQuery ($db, $query);					
					$row = mysqli_fetch_array ( $res );
				?>    
	    <tr id="data">				
				<td>0-17</td>
				<td><?php
					echo ($row [0]);
					?></td>				
		</tr>

   		 		<?php mysqli_free_result ( $res ); ?>
   		 		<?php
					$query = "SELECT COUNT(*) FROM users WHERE users.age >= 18 AND users.age <= 29";
					$res = getQuery ($db, $query);					
					$row = mysqli_fetch_array ( $res );
				?>    
	    <tr id="data">				
				<td>18-29</td>
				<td><?php
					echo ($row [0]);
					?></td>				
		</tr>

   		 		<?php mysqli_free_result ( $res ); ?>
   		 		<?php
					$query = "SELECT COUNT(*) FROM users WHERE users.age >= 30 AND users.age <= 49";
					$res = getQuery ($db, $query);					
					$row = mysqli_fetch_array ( $res );
				?>    
	    <tr id="data">				
				<td>30-49</td>
				<td><?php
					echo ($row [0]);
					?></td>				
		</tr>

   		 		<?php mysqli_free_result ( $res ); ?>
   		 		<?php
					$query = "SELECT COUNT(*) FROM users WHERE users.age >= 50";
					$res = getQuery ($db, $query);					
					$row = mysqli_fetch_array ( $res );
				?>    
	    <tr id="data">				
				<td>50+</td>
				<td><?php
					echo ($row [0]);
					?></td>				
		</tr>

   		 		<?php mysqli_free_result ( $res ); ?>
    </table>
    <br/>
    <hr>
    <br/>
    <h4>Summary by sport</h4>
	<table class="table">
		<tr id="header">
			<th>#</th>
			<th>Sport</th>
			<th>Preferences</th>			
		</tr>		
				<?php
					//$query = "SELECT * FROM count_sport ORDER BY `#ofpeople` DESC";
					$query = "select `sport` AS `sport`, count(*) AS `#ofpeople` from `user_sport` group by `sport` order by `#ofpeople` DESC";
					$res = getQuery ($db, $query);
					
					$row = mysqli_fetch_array ( $res );
					$i=0;
					while ( $row != NULL ) {
				?>    
	    <tr id="data">
				<td><?php 
					echo ($i);
					$i++;
					?></td>
				<td><?php
					echo ($row ["sport"]);
					?></td>
				<td><?php
					echo ($row ["#ofpeople"]);
					?></td>				
		</tr>

    <?php
				$row = mysqli_fetch_array ( $res );
			}
			
			mysqli_free_result ( $res );
			?>
    </table>
	<br/>
	<br/>
	</div>
</body>
</html>