<?php
include 'functions.php';
if (! isset ( $_COOKIE ['testCookie'] )) {
	setcookie ( 'testCookie', 'enabled' );
	$dest = buildNewDestUrlSource ( $_SERVER, "check.php" );
	header ( "Location: " . $dest );
}
if (! checkSession ()) {
	$dest = buildNewDestUrl ( $_SERVER, "index.php" );
	header ( "Location: " . $dest );
}
setcookie('source','sports.php');
if(isset($_COOKIE["https"]) && $_COOKIE["https"] == "false")
{
	setcookie("https","false",time()-60*60);
	toHttp();	
}
// setcookie("https","true");
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title>Sport Survey</title>
<script type="text/javascript" src="MyScripts.js"></script>
<link href="MyStyle.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="navbar">
		<ul class="navbar">
			<li><a id="navlink" href="index.php">Personal Information</a></li>
			<li><a id="navlink" href="sports.php">Sports</a></li>
			<li><a id="navlink" href="email.php">Email and reward</a></li>			
		</ul>
	</div>
	<div class="header">
		<div id="title">
			<h1>Sport Survey Site</h1>
		</div>
		<div id="page">
			<h2>Sport selection</h2>
		</div>
	</div>
	<noscript id="alert">Sorry, your browser does not support or has disabled
		Javascript! Please consider changing browser or turning it back on.</noscript>
	<div class="content">
	<p>Please enter one, two or three sports that you practise</p>
	<p><em>E.g. Tennis, soccer, football, basket, swim, etc.</em></p>
	<form action="validate.php" method="get" class="login" name="sports" onsubmit="return checkSports('sports')">
				<h3>Sports</h3>
				<label>
					<span>First sport: </span>
					<input type="text" id="firstSport" name="firstSport" placeholder="Insert your first sport" title="Insert your first sport"
					value=<?php if(isset($_COOKIE["firstSport"]))
						   					echo('"'.$_COOKIE["firstSport"].'"');
						   				else 
						   					echo('');
						   		 ?>>
				</label>				
				<label>
					<span>Second sport: </span>
					<input type="text" id="secondSport" name="secondSport" placeholder="Insert your second sport" title="Insert your second sport"
					value=<?php if(isset($_COOKIE["secondSport"]))
						   					echo('"'.$_COOKIE["secondSport"].'"');
						   				else 
						   					echo('');
						   		 ?>>
				</label>
				<label>
					<span>Third sport: </span>
					<input type="text" id="thirdSport" name="thirdSport" placeholder="Insert your third sport" title="Insert your third sport"
					value=<?php if(isset($_COOKIE["thirdSport"]))
						   					echo('"'.$_COOKIE["thirdSport"].'"');
						   				else 
						   					echo('');
						   		 ?>>
				</label>
				<input type="hidden" name="source" value="sports.php">
				<input type="submit" name="back" id="back" value="Go Back">
				<input type="submit" name="confirm" id="confirm" value="Confirm">
				
			</form>

	</div>
</body>
</html>