<?php
// some "variables"
$db = "s220352";
//$MAX_PARTICIPANTS = 100;
$expire_time = 120;
function buildNewDestUrl($server, $destPath) {	
	$actual_link = "http://$server[HTTP_HOST]$server[REQUEST_URI]";
	$result = explode ( '/', $actual_link, - 1 );
	$new_link = "";
	foreach ( $result as $item ) {
		if (! strcmp ( $new_link, "" ))
			$new_link = $new_link . $item;
		else
			$new_link = $new_link . "/" . $item;
	}
	$new_link = $new_link . "/" . $destPath;
	return $new_link;
}
function isValid($email)
{
	global $db;
	$conn = dbConnect($db);
	$query = "SELECT COUNT(*) FROM users WHERE email = '". $email ."'";
	$ris = getQuery($db, $query);
	$res = mysqli_fetch_row($ris);
	if($res[0] == 0){
		mysqli_free_result ( $ris );
		mysqli_close ( $conn );
		return true;
	}
	mysqli_free_result ( $ris );
	mysqli_close ( $conn );
	return false;
}
function buildNewDestUrlSource($server, $destPath) {	
	$actual_link = "http://$server[HTTP_HOST]$server[REQUEST_URI]";
	$result = explode ( '/', $actual_link );
	$new_link = "";
	$n = count ( $result );
	$i = 0;
	foreach ( $result as $item ) {
		$i ++;
		if ($i < $n) {
			if (! strcmp ( $new_link, "" ))
				$new_link = $new_link . $item;
			else
				$new_link = $new_link . "/" . $item;
		} else
			break;
	}
	$new_link = $new_link . "/" . $destPath . "?source=" . $item;
	return $new_link;
}
function getHomeUrl($server) {	
	$actual_link = "http://$server[HTTP_HOST]$server[REQUEST_URI]";
	$result = explode ( '/', $actual_link, - 1 );
	$new_link = "";
	foreach ( $result as $item ) {
		if (! strcmp ( $new_link, "" ))
			$new_link = $new_link . $item;
		else
			$new_link = $new_link . "/" . $item;
	}
	$new_link = $new_link . "/index.php";
	return $new_link;
}
function dbConnect($db) {
	$conn = mysqli_connect ( NULL, "root", "", $db );
	//$conn = mysqli_connect ( "localhost", "s220352", "onsundat", $db, 1 );
	if (! $conn)
		die ( "Errore nella connessione al database (" . mysqli_connect_errno () . "): " . mysqli_connect_error () );
	if (! mysqli_set_charset ( $conn, "utf8" ))
		die ( 'Errore nel caricamento del set di caratteri utf8: ' . mysqli_error ( $conn ) );
	return $conn;
}

function clearCookies()
{
	if(isset($_COOKIE["firstName"]))
		setcookie("firstName","",time()-60*60);
	if(isset($_COOKIE["lastName"]))
		setcookie("lastName","",time()-60*60);
	if(isset($_COOKIE["age"]))
		setcookie("age","",time()-60*60);
	if(isset($_COOKIE["firstSport"]))
		setcookie("firstSport","",time()-60*60);
	if(isset($_COOKIE["secondSport"]))
		setcookie("secondSport","",time()-60*60);
	if(isset($_COOKIE["thirdSport"]))
		setcookie("thirdSport","",time()-60*60);
	if(isset($_COOKIE["email"]))
		setcookie("email","",time()-60*60);
	if(isset($_COOKIE["reward"]))
		setcookie("reward","",time()-60*60);
}
/*function utenteValido($db, $user, $password) {
	$conn = dbConnect ( $db );
	$res = mysqli_query ( $conn, "SELECT pwd FROM users WHERE usr = '" . $user . "'" );
	
	if (! $res)
		return false;
	
	if (mysqli_num_rows ( $res ) == 0)
		return false;
	
	$row = mysqli_fetch_array ( $res );
	mysqli_free_result ( $res );
	mysqli_close ( $conn );
	
	if (md5 ( $password ) == $row ["pwd"])
		return true;
	else
		return false;
}
function usernameValid($db, $username) {
	if (! strcmp ( $username, "" ))
		return false;
	$conn = dbConnect ( $db );
	$response = mysqli_query ( $conn, "SELECT COUNT(*) FROM users WHERE usr = '" . $username . "'" );
	$res = $response->fetch_row ();
	mysqli_free_result ( $response );
	mysqli_close ( $conn );
	if ($res == 0)
		return false;
	else
		return true;
}
function insertUser($db, $username, $password) {
	if (! strcmp ( $username, "" ) || ! strcmp ( $password, "" ))
		return false;
	$conn = dbConnect ( $db );
	$newpwd = md5 ( $password );
	
	$insert_row = mysqli_query ( $conn, "INSERT INTO users (usr, pwd) VALUES('$username', '$newpwd')" );
	
	if ($insert_row) {
		mysqli_close ( $conn );
		return true;
	} else {
		mysqli_close ( $conn );
		return false;
	}
}*/
function checkSession() {
	global $expire_time;
	session_start (); // Crea una nuova sessione o ripristina la sessione in corso
	$t = time ();
	$diff = 0;
	
	if (isset ( $_GET ["firstName"] )  && ! empty ( $_GET ["firstName"] )) {
		// L'utente ha appena fatto il login		
			$_SESSION ["S220352user"] = strip_tags($_GET ["firstName"]);
	} else if (! isset ( $_SESSION ["S220352user"] ))
		return false; // Utente non loggato
	
	if (isset ( $_SESSION ['S220352time'] )) {
		$t0 = $_SESSION ['S220352time'];
		$diff = ($t - $t0); // inactivity period
	}
	if ($diff > $expire_time) { // inactivity period too long -> 120 s = 2 min
		session_unset (); // empty session
		session_destroy (); // destroy session
		                   // redirect client to login page
		header ( 'HTTP/1.1 307 temporary redirect' );
		clearCookies();
		setcookie("timeout","expired");
		$dest = buildNewDestUrlSource ( $_SERVER, "index.php" );
		header ( "Location: " . $dest );
		exit ();
	} else
		$_SESSION ['S220352time'] = time (); /* update time */
	
	return true;
}
function getQuery($db, $query) {
	$conn = dbConnect ( $db );
	$ris = mysqli_query ( $conn, $query );
	if (! $ris)
		die ( 'Query non riuscita' );
	mysqli_close ( $conn );
	return $ris;
}
// function formatTimeHhMm($time) {
// 	$sub = explode ( ':', $time, - 1 );
// 	return $sub [0] . ':' . $sub [1];
// }
function toHttps() {
	if ($_SERVER ['SERVER_PORT'] !== 443 && (empty ( $_SERVER ['HTTPS'] ) || $_SERVER ['HTTPS'] === 'off')) {
		header ( 'Location: https://' . $_SERVER ['HTTP_HOST'] . $_SERVER ['REQUEST_URI'] );
		exit ();
	}
}
function toHttp() {
	if  ( isset($_SERVER['HTTPS'] ))
	{
		$host = $_SERVER['HTTP_HOST'];
		$request_uri = $_SERVER['REQUEST_URI'];
		$good_url = "http://" . $host . $request_uri;
	
		header( "HTTP/1.1 301 Moved Permanently" );
		header( "Location: $good_url" );
		exit;
	}
}

// function toMinutes($string){
// 	$sub = explode(":", $string, -1);
// 	$int = $sub[0]*60 + $sub[1];
// 	return $int;
// }

// function isBFeasible($start, $end, $participants){
// 	global $db;
// 	global $MAX_PARTICIPANTS;
// 	$query = "SELECT StartTime, EndTime, NOfParticipants FROM bookings WHERE '".$start."' < EndTime && '".$end."' > StartTime";
// 	$res = getQuery($db, $query);
// 	$i=0;
// 	$row = mysqli_fetch_array($res);
// 	while($row != NULL)
// 	{
// 		$overlapping[$i] = array($row["StartTime"],$row["EndTime"], $row["NOfParticipants"]);
// 		$i++;
// 		$row = mysqli_fetch_array($res);
// 	}
// 	mysqli_free_result($res);
	
// 	if(!isset($overlapping) || empty($overlapping))
// 		return true;

// 	$start_n = toMinutes($start);
// 	$end_n = toMinutes($end);
	
// 	for($i=$start_n; $i <=$end_n; $i++)
// 	{
// 		$sum=$participants;
// 		foreach ($overlapping as $elem)
// 		{
// 			$es = toMinutes($elem[0]);
// 			$ee = toMinutes($elem[1]);
// 			if($es <= $i && $i <= $ee)	
// 				$sum+=$elem[2];
// 		}
// 		if($sum > $MAX_PARTICIPANTS)
// 			return false;
// 	}
// 	return true;	
// }
?>