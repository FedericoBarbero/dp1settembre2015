<?php
include 'functions.php';
function checkValidity()
{
	if (! checkSession ()) {
		$dest = buildNewDestUrlSource ( $_SERVER, "index.php" );
		header ( "Location: " . $dest );
	}
	if(!isset($_GET['firstName']) || !isset($_GET['lastName']) || !isset($_GET['age']) || empty($_GET['firstName'])|| empty($_GET['lastName'])|| empty($_GET['age']))
	{
		setcookie("err","0",time() + 2*60);
		$err = buildNewDestUrl($_SERVER, "error2.php");
		header("Location: " . $err);
		return false;
	}
	
	$firstName = strip_tags($_GET['firstName']);
	$lastName = strip_tags($_GET['lastName']);
	$age = strip_tags($_GET['age']);
	
	if (!preg_match("/^[a-zA-Z ]*$/",$firstName) || !preg_match("/^[a-zA-Z ]*$/",$lastName)) {
		setcookie("err","0",time() + 2*60);
		$err = buildNewDestUrl($_SERVER, "error2.php");
		header("Location: " . $err);
		return false;
	}

	if(is_nan($age) || $age <=0 || $age>=120)
	{
		setcookie("err","1",time()+2*60);
		$err = buildNewDestUrl($_SERVER, "error2.php");
		header("Location: " . $err);
		return false;
	}

	setcookie("firstName",$firstName);
	setcookie("lastName",$lastName);
	setcookie("age",$age);
	return true;
}
function checkEmail(){
	$email;
	if(isset($_GET["email"]) && !empty($_GET['email']))
	{
		$email = strip_tags($_GET["email"]);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			setcookie("err","3",time() + 2*60);
			$err = buildNewDestUrl($_SERVER, "error2.php");
			header("Location: " . $err);
			return false;
		}
	}
	else 
	{
		setcookie("err","3",time() + 2*60);
		$err = buildNewDestUrl($_SERVER, "error2.php");
		header("Location: " . $err);
		return false;
	}
	setcookie("email",$email);
	if(isset($_GET["back"]))
	{
		$dest = buildNewDestUrl($_SERVER, "sports.php");
		header("Location: " . $dest);
		exit();
	}
	return true;
}
function checkSports()
{
	$count = 0;
	$first;
	$second;
	$third;
	if(isset($_GET['firstSport']) && !empty($_GET['firstSport']))
	{
		$count ++;
		$first = strip_tags($_GET['firstSport']);
		setcookie("firstSport",$first);
	}
	if(isset($_GET['secondSport']) && !empty($_GET['secondSport']))
	{
		$count ++;
		$second = strip_tags($_GET['secondSport']);
		setcookie("secondSport",$second);
	}
	if(isset($_GET['thirdSport']) && !empty($_GET['thirdSport']))
	{
		$count ++;
		$third = strip_tags($_GET['thirdSport']);
		setcookie("thirdSport",$third);
	}
	
	if($count === 0)
	{
		setcookie("err","2",time() + 2*60);
		$err = buildNewDestUrl($_SERVER, "error2.php");
		header("Location: " . $err);
		return false;
	}
	if(isset($_GET["back"]))
	{
		$dest = buildNewDestUrl($_SERVER, "index.php");
		header("Location: " . $dest);
		exit();
	}
	return true;
}
function storeDB()
{
	$email;
	if(isset($_GET["email"]) && !empty($_GET['email']))
	{
		$email = strip_tags($_GET["email"]);
	}
	if(!isValid($email))
	{
		setcookie("repeatedEmail","true");
		$dest = buildNewDestUrl($_SERVER, "email.php");
		header("Location: " . $dest);
		exit();
	}
	else 
	{
		global $db;
		$conn = dbConnect($db);
		$firstName; 
		$lastName;
		$age;
		$sport1;
		$sport2;
		$sport3;
		
		if(isset($_COOKIE["firstName"]))
		{
			$firstName = mysqli_real_escape_string($conn,$_COOKIE["firstName"]);
		}
		else 
		{
			echo("ERROR!!!!");
			exit();
		}
		if(isset($_COOKIE["lastName"]))
		{
			$lastName = mysqli_real_escape_string($conn,$_COOKIE["lastName"]);
		}
		else
		{
			echo("ERROR!!!!");
			exit();
		}
		if(isset($_COOKIE["age"]))
		{
			$age = mysqli_real_escape_string($conn,$_COOKIE["age"]);
		}
		else
		{
			echo("ERROR!!!!");
			exit();
		}
		$count = 0;
		$one = false;
		$two = false;
		$three = false;
		if(isset($_COOKIE["firstSport"]))
		{
			$sport1 = strtolower(mysqli_real_escape_string($conn,$_COOKIE["firstSport"]));
			$count++;
			$one = true;
		}		
		if(isset($_COOKIE["secondSport"]))
		{
			$sport2 = strtolower(mysqli_real_escape_string($conn,$_COOKIE["secondSport"]));
			$count++;
			$two = true;
		}
		if(isset($_COOKIE["thirdSport"]))
		{
			$sport3 = strtolower(mysqli_real_escape_string($conn,$_COOKIE["thirdSport"]));
			$count++;
			$three = true;
		}		
		if($count <= 0)
		{
			echo("ERROR!!!!");
		}
		
		$query = "SELECT COUNT(*) FROM users";
		$res = getQuery ($db, $query);
		$row = mysqli_fetch_array ( $res );
		$i = $row[0];
		//echo('i: ' . $i . 'row: ' .$row[0]);
		$sum = 50.0;
		for($j=0;$j<$i && $j<=6;$j++)
			$sum = $sum/2;
		if($sum < 1.0)
			$sum = 1;
		mysqli_free_result($res);
		setcookie("reward",$sum);
		$query = "INSERT INTO users(email, name, surname, age, reward) VALUES ('".$email."','".$firstName."','".$lastName."','".$age."','".$sum."')";
		$ris = getQuery($db, $query);
		if(!$ris)		
			echo("ERROR");
		//mysqli_free_result($ris);
		if($one)
		{
			$query = "INSERT INTO user_sport(email,sport) VALUES ('".$email."','".$sport1."')";
			$ris = getQuery($db, $query);
			if(!$ris)
				echo("ERROR");
			//mysqli_free_result($ris);
		}
		if($two)
		{
			$query = "INSERT INTO user_sport(email,sport) VALUES ('".$email."','".$sport2."')";
			$ris = getQuery($db, $query);
			if(!$ris)
				echo("ERROR");
			//mysqli_free_result($ris);
		}
		if($three)
		{
			$query = "INSERT INTO user_sport(email,sport) VALUES ('".$email."','".$sport3."')";
			$ris = getQuery($db, $query);
			if(!$ris)
				echo("ERROR");
			//mysqli_free_result($ris);
		}
		//clearCookies();
		return;
	}	
}

if(isset($_COOKIE['source']))
{
	$source = $_COOKIE['source'];	
}
else 
{
	setcookie("err","-1",time() + 2*60);
	$err = buildNewDestUrl($_SERVER, "error2.php");
	header("Location: " . $err);
}

switch($source){
	case "index.php":	if(checkValidity())
						{
							$dest = buildNewDestUrl($_SERVER, "sports.php");
							header("Location: " . $dest);
						}
						break;
	case "sports.php":	if(checkSports())
						{
							$dest = buildNewDestUrl($_SERVER, "email.php");
							header("Location: " . $dest);
						}
						break;
	case "email.php":	
						if(checkEmail())
						{
							storeDB();
							$dest = buildNewDestUrl($_SERVER, "statistics.php");
							header("Location: " . $dest);
						}
						break;
	default:			setcookie('err',-1,time()+2*60);
						$err = buildNewDestUrl($_SERVER, "error2.php");
						header("Location: " . $err);
}

?>