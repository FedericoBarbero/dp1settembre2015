<?php include 'functions.php';?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title>Sport Survey</title>
<script type="text/javascript" src="MyScripts.js"></script>
<link href="MyStyle.css" rel="stylesheet" type="text/css">
<style>
h3 {
	color: red;
	font-family: Verdana, Arial, sans-serif;
}
</style>
<script type="text/javascript">
			function backBtOnClick() {
				var home = document.getElementById("url").value;
				window.location.assign(home);
			}
		</script>
</head>
<body>
	<div class="header">
		<div id="title">
			<h1>Sport Survey Site</h1>
		</div>
		<div id="page">
			<h2>Error!</h2>
		</div>
	</div>
	<div id="content">
		<h3>In order to get the site work properly, please enable cookies!</h3>
		<script>
		 document.write('<button id="back" name="back" onclick="backBtOnClick();">Go to the	Home Page</button>');
		</script>
		<noscript id="alert">Sorry, your browser does not support or has disabled
			Javascript! Please consider changing browser or turning it back on.</noscript>
						<?php
						$url = getHomeUrl ( $_SERVER );
						echo ('<input type="hidden" name="url" id="url" value="' . $url . '">');
						
						?>
				
			</div>
</body>
</html>