/***********************************************************
 * JS file created by Federico Barbero - Matricola 220352  *
 ***********************************************************/
//function deleteSelected(formname, checktoggle)
//{
//  var checkboxes = new Array(); 
//  checkboxes = document[formname].getElementsByTagName('input');
// 
//  for (var i=0; i<checkboxes.length; i++)  {
//    if (checkboxes[i].type == 'checkbox')   {
//      checkboxes[i].checked = checktoggle;
//    }
//  }
//}
//
//function checkNewBooking(){
//	var part = document.forms["new_booking"]["#OfParticipants"].value;
//	var startH = parseInt(document.forms["new_booking"]["startHour"].value);
//	var startM = parseInt(document.forms["new_booking"]["startMinutes"].value);
//	var endH = parseInt(document.forms["new_booking"]["endHour"].value);
//	var endM = parseInt(document.forms["new_booking"]["endMinutes"].value);
//	var start = startH*60+startM;
//	var end = endH*60+endM;
//	if(part == null || part=="" || isNaN(part) || part < 0 || part >100 || end <= start){
//		alert("Incorrect input data!");
//		return false;
//	}
//	
//}

function checkPersonalInfo(myForm){
	var x = document.forms[myForm]["firstName"].value;
    if (x == null || x == "") {
        alert("First Name must be filled out");
        return false;
    }
    var x = document.forms[myForm]["lastName"].value;
    if (x == null || x == "") {
        alert("Last Name must be filled out");
        return false;
    }
    var x = document.forms[myForm]["age"].value;
    if (x == null || x == "") {
        alert("Age must be filled out");
        return false;
    }
    if (x <= 0 || x >= 120) {
        alert("Wrong age value! Please check it.");
        return false;
    }
    return true;
}
function checkSports(myForm){
	var count = 3;
	var x = document.forms[myForm]["firstSport"].value;
    if (x == null || x == "") {
        //alert("First Name must be filled out");
    	count --;
        //return false;
    }
    var x = document.forms[myForm]["secondSport"].value;
    if (x == null || x == "") {
        //alert("Last Name must be filled out");
        count --;
        //return false;
    }
    var x = document.forms[myForm]["thirdSport"].value;
    if (x == null || x == "") {
        //alert("Age must be filled out");
    	count --;
        //return false;
    }
    if (count <= 0) {
        alert("Error! Please enter at least one sport");
        return false;
    }
    return true;
}
function checkEmail(myForm){
	var x = document.forms[myForm]["email"].value;
    if (x == null || x == "") {
        alert("Email must be filled out");
        return false;
    }
    return true;
}